# PKHeX Mirror
This branch contains the continuous integration for mirroring the latest PKHeX commits on a nightly basis. Commits are pulled from [`upstream's primary branch`](https://github.com/kwsch/PKHeX/) and pushed to [`this repo's main`](../../tree/main).
